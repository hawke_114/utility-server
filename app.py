import os
import time
import pyotp
from flask import abort, Flask, flash, request, redirect, url_for, render_template, session
from flask import make_response, send_file, Response, send_from_directory
from werkzeug.utils import secure_filename
from hashlib import sha256
import json
import pyqrcode
from flask_saml2.sp import ServiceProvider
from flask_saml2.utils import certificate_from_file, private_key_from_file
from werkzeug.middleware.proxy_fix import ProxyFix

app = Flask(__name__)
app.wsgi_app = ProxyFix(app.wsgi_app, x_proto=1)

#example secret key
#generate new key before ceployment
#
#import os
#os.urandom(24)
#

class KeyCloak(ServiceProvider):
    def get_logout_return_url(self):
        return url_for('uploaded_file', _external=True, _scheme='https')

    def get_default_login_return_url(self):
        return url_for('uploaded_file', _external=True, _scheme='https')

sp = KeyCloak()
app.config['SERVER_NAME'] = 'cloud.dudlo.net'
app.config['SAML2_SP'] = {
    'certificate': certificate_from_file("/var/www/html/content/upload/keys/client-cert.cer"),
    'private_key': private_key_from_file("/var/www/html/content/upload/keys/client_key.key"),
    'entity_id': 'https://cloud.dudlo.net/content/metadata.xml',
}

app.config['SAML2_IDENTITY_PROVIDERS'] = [
    {
        'CLASS': 'flask_saml2.sp.idphandler.IdPHandler',
        'OPTIONS': {
            'display_name': 'My Identity Provider',
            'entity_id': 'https://idp.dudlo.net/auth/realms/master',
            'sso_url': 'https://idp.dudlo.net/auth/realms/master/protocol/saml',
            'slo_url': 'https://idp.dudlo.net/auth/realms/master/protocol/saml',
            'certificate': certificate_from_file('/var/www/html/content/upload/keys/idp_certificate.cer'),
        },
    },
]

auth_user = 'ENTER USERNAME HERE'

app.secret_key = 'change key'

APP_ROOT = app.root_path
SHAREX_FOLDER = os.path.join(APP_ROOT, 'images')
UPLOAD_FOLDER = os.path.join(APP_ROOT, 'files')
ALLOWED_EXTENSIONS = {'txt', 'pdf', 'png', 'jpg', 'jpeg', 'gif', 'cap', 'pcap', 'zip','mp4'}
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
app.config['SHAREX_FOLDER'] = SHAREX_FOLDER
UPLOADS_PASSWORD_HASH = 'ef92b778bafe771e89245b89ecbc08a44a4e166c06659911881f383d4473e94f'
# SHA256 password Hash


def gensecret():
    if not os.path.exists(os.path.join(APP_ROOT, '.secret')):
        with open(os.path.join(APP_ROOT, '.secret'), 'w') as secretfile:
            secretfile.write(pyotp.random_base32())


def readsecret():
    with open(os.path.join(APP_ROOT, '.secret'), 'r') as secretfile:
        secret = secretfile.readline()
    return secret


def check_auth(cookie, current_time, age):
    if session.get(cookie) and current_time - int(session.get('timestamp')) < age:
        return True
    return False


@app.route('/uploads/')
def uploaded_file():
    if sp.is_user_logged_in():
        auth_data = sp.get_auth_data_in_session()
        if auth_data.nameid == auth_user:
            filelist = os.listdir(UPLOAD_FOLDER)
            return render_template('files.html', files=filelist)
    return redirect(url_for('login'))


@app.route('/logout')
def logout():
    session.clear()
    return redirect(url_for('login'))


@app.route('/uploads/<filename>')
def download(filename):
    if sp.is_user_logged_in():
        auth_data = sp.get_auth_data_in_session()
        if auth_data.nameid == auth_user:
            return send_from_directory(app.config['UPLOAD_FOLDER'], filename)
    return redirect(url_for('login'))


@app.route('/login', methods=['GET', 'POST'])
def login():
    message = '<p>You are logged out.</p>'
    login_url = url_for('flask_saml2_sp.login')
    link = f'<p><a href="{login_url}">Log in to continue</a></p>'
    return message + link




app.register_blueprint(sp.create_blueprint(), url_prefix='/saml/')

def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS


@app.route('/', methods=['GET', 'POST'])
def upload_file():
    print(APP_ROOT)
    if request.method == 'POST':
        with open(os.path.join(APP_ROOT, '.secret'), 'r') as file:
            otpSecret = file.readline()
        totp = pyotp.TOTP(otpSecret)
        otp = totp.now()
        code = request.form['code']
        if not check_auth('otpauth', time.time(), 300):
            if code != otp:
                return render_template('base.html', error=True)
        # check if the post request has the file part
        if 'file' not in request.files:
            flash('No file part')
            return redirect(request.url)
        file = request.files['file']
        # if user does not select file, browser also
        # submit an empty part without filename
        session['otpauth'] = True
        session['timestamp'] = time.time()
        if file.filename == '':
            flash('No selected file')
            return render_template('base.html', error=True)
        if file and allowed_file(file.filename):
            filename = secure_filename(file.filename)
            file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
            return render_template('base.html', error=False, filename=file.filename)
    if check_auth('otpauth', time.time(), 300):
        return render_template('base.html', otpauth=True)
    session.clear()
    if not os.path.exists(os.path.join(APP_ROOT, '.secret')):
        try:
            gensecret()
            secret = readsecret()
        except PermissionError:
            return Response('Cound not create Secret file, check permissions')
        otp = pyotp.TOTP(secret)
        qr = pyqrcode.create(otp.provisioning_uri(name='Utility Server', issuer_name='Flask'))
        qr.svg(os.path.join(APP_ROOT, 'qr.svg'), scale=8)
        with open(os.path.join(APP_ROOT, 'qr.svg'), 'r') as file:
            qrcode = file.read()
        return render_template('qrcode.html', qrcode=qrcode)

    return render_template('base.html')


@app.route('/share', methods=['POST'])
def sharex():
    file = request.files['upload-file']
    filename = file.filename
    auth_header = request.headers.get('x-auth')
    auth_header_hash = sha256(auth_header.encode()).hexdigest()
    if auth_header_hash == '05cafa178b6cd0a13e3b5109bc3c49a573ba041bc81d73b63d76e2e39e9569fe':
        file.save(os.path.join(app.config['SHAREX_FOLDER'], filename))
        return redirect(url_for('upload_file') + 'images/' + filename)
    return abort(404)


@app.route('/images/<filename>')
def images(filename):
    return send_from_directory(SHAREX_FOLDER, filename)


@app.route('/proxyinfo')
def proxyinfo():
    headers = request.headers
    info = {'headers': headers.to_wsgi_list(), 'ip': request.remote_addr}
    json_out = json.dumps(info, sort_keys=True, indent=4)
    response = make_response(Response(json_out, mimetype='application/json'))
    response.headers['Cache-Control'] = 'no-cache'
    response.headers['Pragma'] = 'no-cache'
    return response


@app.route('/pacfile')
def pacfile():
    pac_name = request.args.get('pac')
    pac_path = os.path.join(UPLOAD_FOLDER, pac_name + '.pac')
    if os.path.exists(pac_path):
        return send_file(pac_path, mimetype='application/x-ns-proxy-autoconfig', attachment_filename='proxy.pac',
                         as_attachment=True)
    else:
        return abort(404)

#@app.route('/bitlockerstate')
#def bitlockerstate() :
#    hostname = request.args.get('hostname')
#    status = request.args.get('status')
#    report_path = os.path.join(UPLOAD_FOLDER, 'bitlocker.json')
#    with open(report_path, 'r') as datafile :
#        data = json.loads(datafile.read())
#    data.update({hostname : {'time' : time.strftime('%m/%d %H:%M'), 'status' : status}})
#    with open(report_path, 'w') as datafile :
#        datafile.write(json.dumps(data, indent=2))
#    return 'success'
    
 
@app.route('/otpqr')
def otpqr():
    # authenticated = session.get('authenticated')
    #   UNCOMMENT THESE LINES TO ACCESS THE OTP QR CODE IN THE BROWSER
    #    if authenticated:
    #        otp = pyotp.TOTP(otpSecret)
    #        qr = pyqrcode.create(otp.provisioning_uri('utilityserver', issuer_name='flask'))
    #        qr.svg(os.path.join(app.root_path, 'qr.svg'), scale=4)
    #        return send_file('qr.svg')
    return redirect(url_for('login'))


if __name__ == '__main__':
    app.run()

