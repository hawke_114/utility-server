# Utility Server
Flask app to support engineers working with customers

## Features

- '/' directory allows the uploading of files. Authentication occurs by way of one-time passwords. A QR code is
generated during the first run of the application

- '/uploads' directory allows the download of the files that were uploaded

- '/proxyinfo' shows HTTP request headers

- '/share' endpoint allows uploads to the flask app via sharex

- '/pacfile' endpoint for hosting pacfiles. Can serve them by URL Query string
## Usage Instructions

Recommended to be installed on a small cloud VM instance. (GCP or AWS free tier would work)

1. clone the repo
2. create a directory called 'files', and 'images'
3. Edit ```app.py``` and set the authentication hashes to use the password of your choice
4. to run the flask app:
    - in standalone debug mode for one-off usage run ```run.py```
    - Installed on a production web server follow the directions at 
    https://flask.palletsprojects.com/en/1.1.x/tutorial/deploy/
 