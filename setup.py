from setuptools import setup, find_packages

setup(
    name='utility_server',
    version='0.1',
    packages=[],
    install_requires=['flask', 'pyotp', 'PyQRCode'],
    url='',
    license='',
    author='Matt.Dudlo',
    author_email='matt.dudlo@gmail.com',
    description='Flask server with some useful utilities'
)
